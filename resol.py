#!/usr/bin/env python3

import numpy as np

L = 0.50
dL = 0.005

N = 30
g = 9.79199
dg = 0.000005

Tteorico = 2*np.pi*np.sqrt(L/g)
dTteorico = 0.1

np.random.seed(0)
T = np.round(np.random.randn(N)*dTteorico + Tteorico, 2)
